import React from 'react';
import './ShoppingList.css';

const ShoppingList = props => {
  const purchases = props.list.map(purchase => {
    return (
      <div className="Purchase" key={purchase.id}>
        <span className="PurchaseName">{purchase.name}</span>
        <span className="PurchaseRemove" title="Remove" onClick={() => props.remove(purchase.id)}>X</span>
        <span className="PurchaseCost">{purchase.cost} KGS</span>
      </div>
    );
  });

  const totalSpent = props.list.reduce((sum, i) => sum + i.cost, 0);

  return (
    <div className="ShoppingList">
      {purchases}
      <p className="TotalSpent top">Total spent:</p>
      <p className="TotalSpent">{totalSpent} KGS</p>
    </div>
  );
};

export default ShoppingList;