import React from 'react';
import './NewPurchase.css';

const NewPurchase = props => {

  return (
    <div className="FormBlock">
      <input type="text" className="InputName" name="name" placeholder="Item name"
             value={props.value.name}
             onChange={(event) => props.change(event)}/>
      <input type="text" className="InputCost" name="cost" placeholder="Cost"
             value={props.value.cost}
             onChange={(event) => props.change(event)}/>
      <span className="Currency">KGS</span>
      <button type="button" className="Btn" onClick={() => props.add()}>Add</button>
    </div>
  );
};

export default NewPurchase;