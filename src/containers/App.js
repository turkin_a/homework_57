import React, { Component } from 'react';
import './App.css';
import NewPurchase from "../components/NewPurchase";
import ShoppingList from "../components/ShoppingList";

let idNum = 0;

class App extends Component {
  state = {
    purchases: [],
    newPurchase: {
      name: '',
      cost: ''
    }
  };

  addNewPurchase = () => {
    const purchases = [...this.state.purchases];
    const newPurchase = {...this.state.newPurchase};

    if (isNaN(newPurchase.cost) || newPurchase.cost === '' || parseInt(newPurchase.cost, 10) <= 0) return null;

    purchases.push({
      name: newPurchase.name,
      cost: parseInt(newPurchase.cost, 10),
      id: idNum
    });

    newPurchase.name = '';
    newPurchase.cost = '';
    idNum++;

    this.setState({purchases, newPurchase});
  };

  handleInputChange = (event) => {
    const target = event.target;
    const newPurchase = {...this.state.newPurchase};
    newPurchase[target.name] = target.value;

    this.setState({newPurchase})
  };

  removePurchase = (id) => {
    const index = this.state.purchases.findIndex(p => p.id === id);
    const purchases = [...this.state.purchases];

    purchases.splice(index, 1);

    this.setState({purchases});
  };

  render() {
    return (
      <div className="App">
        <NewPurchase
          value={this.state.newPurchase}
          change={this.handleInputChange}
          add={this.addNewPurchase}
        />
        <ShoppingList
          list={this.state.purchases}
          remove={this.removePurchase}
        />
      </div>
    );
  }
}

export default App;